<!DOCTYPE html>
<html lang="en">

<head>
    <title>BPS Aceh</title>

    <!-- Meta -->
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0, user-scalable=0, minimal-ui">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="description" content="#BPS1100">
    <meta name="keywords" content="BPS1100">
    <meta name="author" content="#BPS1100">
    <!-- Favicon icon -->
    <link rel="icon" href="<?= base_url(); ?>\files\assets\images\logo_bps.png" type="image/x-icon">
    <!-- Google font-->
    <link href="https://fonts.googleapis.com/css?family=Open+Sans:400,600,800" rel="stylesheet">
    <!-- Required Fremwork -->
    <link rel="stylesheet" type="text/css"
        href="<?= base_url(); ?>\files\bower_components\bootstrap\css\bootstrap.min.css">

    <!-- feather Awesome -->
    <link rel="stylesheet" type="text/css" href="<?= base_url(); ?>\files\assets\icon\feather\css\feather.css">
    <!-- Font Awesome -->
    <link rel="stylesheet" type="text/css"
        href="<?= base_url(); ?>\files\assets\icon\font-awesome\css\font-awesome.min.css">
    <!-- Data Table Css -->
    <link rel="stylesheet" type="text/css"
        href="<?= base_url(); ?>\files\bower_components\datatables.net-bs4\css\dataTables.bootstrap4.min.css">
    <!-- <link rel="stylesheet" type="text/css"
        href="<?= base_url(); ?>\files\assets\pages\data-table\css\buttons.dataTables.min.css">
    <link rel="stylesheet" type="text/css"
        href="<?= base_url(); ?>\files\bower_components\datatables.net-responsive-bs4\css\responsive.bootstrap4.min.css"> -->

    <!-- Style.css -->
    <link rel="stylesheet" type="text/css" href="<?= base_url(); ?>\files\assets\css\style.css">
    <link rel="stylesheet" type="text/css" href="<?= base_url(); ?>\files\assets\css\jquery.mCustomScrollbar.css">
</head>

<body>
    <!-- awal Pre-loader -->
    <?php require 'pre-loader.php'; ?>
    <!-- akhir Pre-loader -->

    <div id="pcoded" class="pcoded">
        <div class="pcoded-overlay-box"></div>
        <div class="pcoded-container navbar-wrapper">

            <nav class="navbar header-navbar pcoded-header">
                <div class="navbar-wrapper">

                    <div class="navbar-logo">
                        <a class="mobile-menu" id="mobile-collapse" href="#!">
                            <i class="feather icon-menu"></i>
                        </a>
                        <a href="index-1.htm">
                            <img class="img-fluid" src="<?= base_url(); ?>\files\assets\images\logo_bps_navbar.png"
                                alt="Theme-Logo">
                        </a>
                        <a class="mobile-options">
                            <i class="feather icon-more-horizontal"></i>
                        </a>
                    </div>

                    <div class="navbar-container container-fluid">
                        <!-- awal nav-left -->
                        <?php require 'nav-left.php'; ?>
                        <!-- akhir nav-left -->

                        <!-- awal nav-right -->
                        <?php require 'nav-right.php';
                        ?>
                        <!-- akhir nav-right -->

                    </div>
                </div>
            </nav>


            <div class="pcoded-main-container">
                <div class="pcoded-wrapper">
                    <!-- awal navbar -->
                    <?php require 'navbar.php'; ?>
                    <!-- akhir navbar -->
                    <div class="pcoded-content">
                        <div class="pcoded-inner-content">
                            <!-- Main-body start -->
                            <div class="main-body">
                                <div class="page-wrapper">
                                    <!-- awal konten -->
                                    <?php
                                    echo $contents;
                                    ?>
                                    <!-- akhir konten -->
                                </div>
                            </div>
                            <!-- Main-body start -->
                            <!-- <div id="styleSelector"> -->


                        </div>
                    </div>
                </div>
            </div>

        </div>
    </div>
    </div>



    <!-- Warning Section Ends -->
    <!-- Required Jquery -->
    <script type="text/javascript" src="<?= base_url(); ?>\files\bower_components\jquery\js\jquery.min.js"></script>
    <script type="text/javascript" src="<?= base_url(); ?>\files\bower_components\jquery-ui\js\jquery-ui.min.js">
    </script>
    <script type="text/javascript" src="<?= base_url(); ?>\files\bower_components\popper.js\js\popper.min.js"></script>
    <script type="text/javascript" src="<?= base_url(); ?>\files\bower_components\bootstrap\js\bootstrap.min.js">
    </script>
    <!-- jquery slimscroll js -->
    <script type="text/javascript"
        src="<?= base_url(); ?>\files\bower_components\jquery-slimscroll\js\jquery.slimscroll.js"></script>
    <!-- modernizr js -->
    <script type="text/javascript" src="<?= base_url(); ?>\files\bower_components\modernizr\js\modernizr.js"></script>
    <script type="text/javascript" src="<?= base_url(); ?>\files\bower_components\modernizr\js\css-scrollbars.js">
    </script>
    <!-- data-table js -->
    <script src="<?= base_url(); ?>\files\bower_components\datatables.net\js\jquery.dataTables.min.js"></script>
    <script src="<?= base_url(); ?>\files\bower_components\datatables.net-buttons\js\dataTables.buttons.min.js">
    </script>
    <!-- <script src="<?= base_url(); ?>\files\assets\pages\data-table\js\jszip.min.js"></script> -->
    <!-- <script src="<?= base_url(); ?>\files\assets\pages\data-table\js\pdfmake.min.js"></script> -->
    <!-- <script src="<?= base_url(); ?>\files\assets\pages\data-table\js\vfs_fonts.js"></script> -->
    <!-- <script src="<?= base_url(); ?>\files\bower_components\datatables.net-buttons\js\buttons.print.min.js"></script> -->
    <!-- <script src="<?= base_url(); ?>\files\bower_components\datatables.net-buttons\js\buttons.html5.min.js"></script> -->
    <script src="<?= base_url(); ?>\files\bower_components\datatables.net-bs4\js\dataTables.bootstrap4.min.js"></script>
    <script src="<?= base_url(); ?>\files\bower_components\datatables.net-responsive\js\dataTables.responsive.min.js">
    </script>
    <script
        src="<?= base_url(); ?>\files\bower_components\datatables.net-responsive-bs4\js\responsive.bootstrap4.min.js">
    </script>

    <!-- i18next.min.js -->
    <!-- <script type="text/javascript" src="<?= base_url(); ?>\files\bower_components\i18next\js\i18next.min.js"></script>
    <script type="text/javascript"
        src="<?= base_url(); ?>\files\bower_components\i18next-xhr-backend\js\i18nextXHRBackend.min.js"></script>
    <script type="text/javascript"
        src="<?= base_url(); ?>\files\bower_components\i18next-browser-languagedetector\js\i18nextBrowserLanguageDetector.min.js">
    </script>
    <script type="text/javascript"
        src="<?= base_url(); ?>\files\bower_components\jquery-i18next\js\jquery-i18next.min.js"></script> -->
    <!-- Custom js -->

    <script src="<?= base_url(); ?>\files\assets\js\pcoded.min.js"></script>
    <script src="<?= base_url(); ?>\files\assets\js\vartical-layout.min.js"></script>
    <script src="<?= base_url(); ?>\files\assets\js\jquery.mCustomScrollbar.concat.min.js"></script>
    <script type="text/javascript" src="<?= base_url(); ?>\files\assets\js\script.js"></script>
    <script src="<?= base_url(); ?>\files\assets\pages\data-table\js\data-table-custom.js"></script>

</body>

</html>