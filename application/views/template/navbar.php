<nav class="pcoded-navbar">
    <div class="pcoded-inner-navbar main-menu">
        <!-- <div class="pcoded-navigatio-lavel">Navigation</div> -->
        <ul class="pcoded-item pcoded-left-item">
            <li class="">
                <a href="navbar-light.htm">
                    <span class="pcoded-micon"><i class="feather icon-home"></i></span>
                    <span class="pcoded-mtext">Dashboard</span>
                </a>
            </li>

            <li class="pcoded-hasmenu">
                <a href="javascript:void(0)">
                    <span class="pcoded-micon"><i class="fa fa-money"></i></span>
                    <span class="pcoded-mtext">Anggaran</span>
                    <!-- <span class="pcoded-badge label label-warning">NEW</span> -->
                </a>
                <ul class="pcoded-submenu">

                    <li class=" ">
                        <a href="<?= base_url('pok'); ?>">
                            <span class="pcoded-mtext">POK</span>
                            <!-- upload excel -->
                        </a>
                    </li>
                    <li class=" ">
                        <a href="box-layout.htm" target="_blank">
                            <span class="pcoded-mtext">RPD</span>
                            <!-- edit excel -->
                        </a>
                    </li>
                    <li class=" ">
                        <a href="menu-rtl.htm" target="_blank">
                            <span class="pcoded-mtext">Realisasi</span>
                            <!-- edit excel -->
                        </a>
                    </li>
                </ul>
            </li>
            <li class="pcoded-hasmenu">
                <a href="javascript:void(0)">
                    <span class="pcoded-micon"><i class="fa fa-sign-language"></i></span>
                    <span class="pcoded-mtext">Kegiatan</span>
                    <!-- <span class="pcoded-badge label label-warning">NEW</span> -->
                </a>
                <ul class="pcoded-submenu">

                    <li class=" ">
                        <a href="menu-bottom.htm">
                            <span class="pcoded-mtext">DIPA</span>
                            <!-- upload pdf -->
                        </a>
                    </li>
                    <li class=" ">
                        <a href="box-layout.htm" target="_blank">
                            <span class="pcoded-mtext">SK</span>
                            <!-- upload pdf -->
                        </a>
                    </li>
                    <li class=" ">
                        <a href="menu-rtl.htm" target="_blank">
                            <span class="pcoded-mtext">KAK</span>
                            <!-- upload pdf -->
                        </a>
                    </li>
                    <li class=" ">
                        <a href="menu-rtl.htm" target="_blank">
                            <span class="pcoded-mtext">Form Permintaan</span>
                            <!-- download template -->
                        </a>
                    </li>
                    <li class=" ">
                        <a href="menu-rtl.htm" target="_blank">
                            <span class="pcoded-mtext">Laporan Pelatihan</span>
                            <!-- upload pdf -->
                        </a>
                    </li>
                    <li class=" ">
                        <a href="menu-rtl.htm" target="_blank">
                            <span class="pcoded-mtext">Laporan Survei</span>
                            <!-- upload pdf -->
                        </a>
                    </li>
                    <li class=" ">
                        <a href="menu-rtl.htm" target="_blank">
                            <span class="pcoded-mtext">Jadwal</span>
                        </a>
                    </li>
                </ul>
            </li>
            <li class="pcoded-hasmenu">
                <a href="javascript:void(0)">
                    <span class="pcoded-micon">
                        <i class="fa fa-book"></i>
                    </span>
                    <span class="pcoded-mtext">SAKIP</span>
                    <!-- <span class="pcoded-badge label label-warning">NEW</span> -->
                </a>
                <ul class="pcoded-submenu">

                    <li class=" ">
                        <a href="<?= base_url('fra'); ?>">
                            <span class="pcoded-mtext">FRA</span>
                            <!-- edit excel -->
                        </a>
                    </li>
                    <li class=" ">
                        <a href="box-layout.htm" target="_blank">
                            <span class="pcoded-mtext">Dokumentasi Rapat</span>
                            <!-- upload pdf -->
                        </a>
                    </li>

                </ul>
            </li>
            <li class="pcoded-hasmenu">
                <a href="javascript:void(0)">
                    <span class="pcoded-micon"><i class="feather icon-monitor"></i></span>
                    <span class="pcoded-mtext">Monitoring</span>
                    <!-- <span class="pcoded-badge label label-warning">NEW</span> -->
                </a>
                <ul class="pcoded-submenu">

                    <li class=" ">
                        <a href="menu-bottom.htm">
                            <span class="pcoded-mtext">PSPA</span>
                            <!-- edit excel -->
                        </a>
                    </li>
                    <li class=" ">
                        <a href="box-layout.htm" target="_blank">
                            <span class="pcoded-mtext">Smart Kemenkeu</span>
                            <!-- edit excel --></a>

                    </li>
                    <li class=" ">
                        <a href="menu-rtl.htm" target="_blank">
                            <span class="pcoded-mtext">MOnev Bappenas</span>
                            <!-- edit excel -->
                        </a>
                    </li>
                </ul>
            </li>
            <li class="">
                <a href="navbar-light.htm">
                    <span class="pcoded-micon"><i class="fa fa-sign-out"></i></span>
                    <span class="pcoded-mtext">Logout</span>
                </a>
            </li>

        </ul>



    </div>
</nav>