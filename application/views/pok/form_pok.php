<div class="card">
    <div class="card-header">
        <h5>Form Inputs POK</h5>
    </div>
    <div class="card-body">
        <?= form_open_multipart('pok/upload', array('id' => 'form'), array('method' => 'post')); ?>
        <div class="form-group row">
            <label class="col-sm-2 col-form-label">Judul POK</label>
            <div class="col-sm-10">
                <input type="text" class="form-control" name="judul_pok" placeholder="Judul POK">
            </div>
        </div>
        <div class="form-group row">
            <label class="col-sm-2 col-form-label">File PDF POK</label>
            <div class="col-sm-10">
                <input type="file" class="form-control" name="file" placeholder="File PDF POK">
            </div>
        </div>
        <button type="submit" class="btn btn-info btn-round">Submit</button>
        <?= form_close(); ?>
    </div>
</div>