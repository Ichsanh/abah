<div class="card">
    <div class="card-header">
        <div class="d-flex align-items-center">
            <h4 class="card-title">Daftar POK</h4>
            <a href="<?= base_url(); ?>pok/upload" class="btn btn-primary btn-round btn-sm ml-auto">

                <i class="fa fa-plus"></i>
                Tambah

            </a>
        </div>
    </div>
    <div class="card-block">
        <div class="dt-responsive table-responsive">
            <table id="simpletable" class="table table-striped table-bordered nowrap">
                <thead>
                    <tr>
                        <th>no</th>
                        <th>Judul</th>
                        <th>Aksi</th>

                    </tr>
                </thead>
                <tbody>
                    <?php

                    $i = 0;
                    foreach ($data as $key => $value) { ?>
                    <tr>
                        <td class="text-center"><?php echo ++$i; ?></td>
                        <td><?= $value['judul_pok']; ?></td>


                        <td class="text-center">
                            <a href="<?= base_url(); ?>pok/edit/<?= $value['id_pok']; ?>">

                                <i class="fa fa-edit" style="color: green" data-original-title="Edit" rel="tooltip"></i>

                            </a>
                            <a href="<?= base_url(); ?>pok/edit/<?= $value['id_pok']; ?>">

                                <i class="fa fa-download" style="color: blue" data-original-title="Download"
                                    rel="tooltip"></i>

                            </a>


                        </td>
                    </tr>
                    <?php } ?>

                </tbody>

            </table>
        </div>
    </div>
</div>