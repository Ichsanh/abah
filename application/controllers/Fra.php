<?php
defined('BASEPATH') or exit('No direct script access allowed');

class Fra extends CI_Controller
{
    function __construct()
    {
        parent::__construct();

        $this->load->model('master_model');
    }

    public function index()
    {
        $data['halaman'] = 'cari_pustaka';

        $this->load->vars($data);
        $this->template->load('template/template', 'welcome');
    }
}