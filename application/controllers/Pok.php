<?php
defined('BASEPATH') or exit('No direct script access allowed');

class Pok extends CI_Controller
{
    function __construct()
    {
        parent::__construct();

        $this->load->model('master_model');
    }

    public function index()
    {
        $data['halaman'] = 'cari_pustaka';

        $data['data'] = $this->master_model->view('pok');

        $this->load->vars($data);

        $this->template->load('template/template', 'pok/data');
    }
    public function upload()
    {
        $this->form_validation->set_rules('judul_pok', 'judul pok', 'required');
        if ($this->form_validation->run()) {
            $this->load->library('upload');


            $config['upload_path'] = './kumpulan_file/pok/'; //path folder
            $config['allowed_types'] = 'gif|jpg|png|jpeg|bmp|pdf'; //type yang dapat diakses bisa anda sesuaikan
            $config['max_size'] = '50000'; //maksimum besar file 2M

            $this->upload->initialize($config);


            $insert = [
                'judul_pok' => $this->input->post('judul_pok', true),
                //    'sys_ins_user_id' => $this->user->id,
            ];
            if (!empty($_FILES['file']['name'])) {
                if ($this->upload->do_upload('file')) {
                    $gbr = $this->upload->data();
                    $insert['nama_file'] = $gbr['file_name'];
                }
            }

            $into_db = $this->master_model->create('pok', $insert);

            redirect('pok', 'refresh');
        } else {
            $data = [
                // 'user' => $this->user,
                'judul'    => 'Mata Kuliah',
                'subjudul' => 'Data Mata Kuliah',
                'menu' => 'anggaran',
                'menu_' => 'master_',
                'sub_menu' => 'pok'
            ];
            $data['subject'] = $this->master_model->view('pok');

            $this->load->vars($data);

            $this->template->load('template/template', 'pok/form_pok');
        }
    }
}