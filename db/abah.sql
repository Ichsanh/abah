-- phpMyAdmin SQL Dump
-- version 4.7.0
-- https://www.phpmyadmin.net/
--
-- Host: 127.0.0.1
-- Generation Time: 09 Sep 2020 pada 17.43
-- Versi Server: 10.1.25-MariaDB
-- PHP Version: 7.0.21

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET AUTOCOMMIT = 0;
START TRANSACTION;
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;

--
-- Database: `abah`
--

-- --------------------------------------------------------

--
-- Struktur dari tabel `pok`
--

CREATE TABLE `pok` (
  `id_pok` int(11) NOT NULL,
  `judul_pok` text COLLATE utf8mb4_unicode_ci NOT NULL,
  `nama_file` text COLLATE utf8mb4_unicode_ci NOT NULL,
  `waktu_upload` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data untuk tabel `pok`
--

INSERT INTO `pok` (`id_pok`, `judul_pok`, `nama_file`, `waktu_upload`) VALUES
(1, 'ada deh', '', '2020-09-09 14:35:43'),
(2, 'ada deh', 'Bukti_Pemesanan_Uang_Peringatan_Kemerdekaan_75_Tahun_RI_48DPXG.pdf', '2020-09-09 14:37:00');

--
-- Indexes for dumped tables
--

--
-- Indexes for table `pok`
--
ALTER TABLE `pok`
  ADD PRIMARY KEY (`id_pok`);

--
-- AUTO_INCREMENT for dumped tables
--

--
-- AUTO_INCREMENT for table `pok`
--
ALTER TABLE `pok`
  MODIFY `id_pok` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=3;COMMIT;

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
